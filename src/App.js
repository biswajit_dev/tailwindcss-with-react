import React, { useEffect, useState } from "react";
import axios from "axios";
import Imagecard from "./Imagecard";
import ImageSearch from "./Search";

const App = () => {
  const [images, setImages] = useState([]);
  const [loading, setLoading] = useState(false);
  const [term, setTerm] = useState("");

  const getImages = () => {
    setLoading(true);
    axios
      .get(
        `https://pixabay.com/api/?key=${process.env.REACT_APP_PIXABAY_API}&q=${term}&image_type=photo&pretty=true`
      )
      .then(({ data }) => {
        setImages(data.hits || []);
        setLoading(false);
      })
      .catch((err) => {
        console.log("Error: ", err); 
        setLoading(false);
      });
  }

  useEffect(() => {
    getImages();
  }, []);

  useEffect(()=> {
    if(!term) return;
    getImages();
  }, [term])

  if (loading) {
    return <div>Loading...</div>;
  }

  return (
    <div className="container mx-auto">
      <ImageSearch searchText={(text) => setTerm(text)} />
      <div className="grid grid-cols-3 gap-4">
        {images.map((el, i) => (
          <Imagecard
            key={i}
            largeImageURL={el.largeImageURL}
            user={el.user}
            views={el.views}
            likes={el.likes}
            downloads={el.downloads}
            tags={el.tags}
          />
        ))}
      </div>
    </div>
  );
};

export default App;
